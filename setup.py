#!/usr/bin/python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="wikicommons2iiifprezi",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="IIIF Manifest from wiki commons data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_query_builder",
    install_requires=['elasticsearch<8', 'requests',
                      'rdv_config_store_ubit', 'iiif_prezi3',
                      'flask', 'flask_cors', 'flask_compress', 'flask-restx',
                      'cache_decorator_redis_ubit', "uwsgi", "iiif_discovery4vitrivr"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"": "logging-decorator"},
    packages=setuptools.find_packages(),
    python_requires=">=3.8",
    include_package_data=True,
    zip_safe=False
)