import json
import os
import requests

from flask import request, Response, Flask
from flask_restx import Resource, Namespace
from flask_cors import CORS
from flask_compress import Compress
from rdv_config_store_ubit import ConfigStore


from wikicommons2iiifprezi.commons2iiifprezi import Wiki2Manifest

ns_iiif = Namespace('iiif', description="return manifest for wiki commons items")

@ns_iiif.route('/commons2prezi/<file>/manifest.json', methods=['GET'])
@ns_iiif.route('/commons2prezi/<file>/manifest.json/', methods=['GET'])
class WikiCommonsIIIFManifest(Resource):

    @classmethod
    def get(self, file=None):
        wiki = Wiki2Manifest(file)
        manifest = wiki.get_wiki_manifest(host=request.base_url)
        r = manifest.json()
        if isinstance(r, dict):
            r = json.dumps(r)
        resp = Response(r)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/json"
        return resp

@ns_iiif.route('/commonsdb2prezi/<file>/manifest.json', methods=['GET'])
@ns_iiif.route('/commonsdb2prezi/<file>/manifest.json/', methods=['GET'])
class WikiCommonsESIIIFManifest(Resource):

    @classmethod
    def get(self, file=None):

        config_store = ConfigStore()
        config_store.load_project_config(project="autographen-prod")

        wiki = Wiki2Manifest(file)
        manifest = wiki.build_wiki_manifestfromdb(config_store=config_store, host=request.base_url)
        r = manifest.json()
        if isinstance(r, dict):
            r = json.dumps(r)
        resp = Response(r)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/json"
        return resp

@ns_iiif.route('/altoxml/<fileid>', methods=['GET'])
@ns_iiif.route('/altoxml/<fileid>/', methods=['GET'])
class EnrichedIIIFManifest(Resource):

    @classmethod
    def get(self, fileid=None):

        file = open("/home/martin/PycharmProjects/wikicommons2iiifprezi/wikicommons2iiifprezi/data/00001.xml").read()
        r = file
        resp = Response(r)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/xml"
        return resp

@ns_iiif.route('/addalto/<vlmid>/manifest.json', methods=['GET'])
@ns_iiif.route('/addalto/<vlmid>/manifest.json/', methods=['GET'])
class EnrichedIIIFManifest(Resource):

    @classmethod
    def get(self, vlmid=None):

        config_store = ConfigStore()
        config_store.load_project_config(project="autographen-prod")
        import requests

        manifest = requests.get(f"https://www.e-manuscripta.ch/i3f/v20/{vlmid}/manifest").json()
        manifest["@id"] = request.base_url
        alto_file = "00001.xml"
        for n, canvas in enumerate(manifest["sequences"][0]["canvases"]):
            canvas["seeAlso"] =[{"label": "Alto", "type": "Dataset", "@id": f"https://localhost:8787/v1/iiif/altoxml/{alto_file}",
                       "format": "application/xml+alto", "profile": "http://www.loc.gov/standards/alto/v4/alto.xsd"}]
            canvas["rendering"] = canvas["seeAlso"]
        r = manifest
        if isinstance(r, dict):
            r = json.dumps(r)
        resp = Response(r)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/json"
        return resp

@ns_iiif.route('/iiifimg/<filename>', methods=['GET'])
@ns_iiif.route('/iiifimg/<filename>/', methods=['GET'])
@ns_iiif.route('/iiifimg/<filename>/info.json', methods=['GET'])
@ns_iiif.route('/iiifimg/<filename>/<path:rest>/default.jpg', methods=['GET'])
class IIIFImgProxy(Resource):

    @classmethod
    def get(self, filename=None, rest=None):
        wiki_iiif_template = "https://zoomviewer.toolforge.org/proxy.php?iiif={}".format(filename)
        if rest:
            passthrough = f"{wiki_iiif_template}/{rest}/default.jpg"
            image = requests.get(passthrough)
            resp = Response(image)
            resp.headers['Access-Control-Allow-Origin'] = '*'
            resp.headers['Content-Type'] = "image/jpeg"
            return resp

        test = requests.get(f"{wiki_iiif_template}/info.json")
        test = test.json()
        test["@id"] = wiki_iiif_template
        from flask import jsonify
        resp = jsonify(test)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/json"
        return resp

from flask import Blueprint
from flask_restx import Api

url_prefix = "/v1"
documentation_endpoint = "/doc/"

# v1 APIs
api_v1 = Blueprint('api_v1', __name__, url_prefix=url_prefix)
home_page = Blueprint('home-page', __name__)

@home_page.route("/")
def index_page():
    return 'See <a href="{}{}">Swagger Documentation</a> of this API'.format(url_prefix, documentation_endpoint)

#from iiif_discovery4vitrivr.iiif_discovery import ns_iiifdiscovery

commonsiiif_api_v1 = Api(api_v1, doc=documentation_endpoint, version="unstable", title='RDV API',
                 description='This is the documentation of the [wikicommons2iiifprezi] Endpoint')
commonsiiif_api_v1.add_namespace(ns_iiif)
#commonsiiif_api_v1.add_namespace(ns_iiifdiscovery)
commonsiiif_app = Flask(__name__)
commonsiiif_app.register_blueprint(api_v1)
commonsiiif_app.register_blueprint(home_page)

# for changes here, copy uwsgi_rdv to destination (simple deployment not working) - mre workflow
origins = [
    "https?://127.0.0.1.*",
    "https?://localhost.*",
    "https?://ub-.*.unibas.ch",
    "https?://192.168.128.*",
    "https?://.*.ub-digitale-dienste.k8s-001.unibas.ch"
]
CORS(commonsiiif_app, origins=origins, supports_credentials=True)
Compress(commonsiiif_app)
