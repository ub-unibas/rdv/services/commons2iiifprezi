import re
import requests
from xml.etree import  ElementTree
import urllib.parse

from elasticsearch import Elasticsearch

from iiif_prezi3 import Manifest, config, KeyValueString, Canvas, ResourceItem, ServiceItem, AnnotationPage, Annotation
from cache_decorator_redis_ubit import CacheDecorator12h


#CORS anschauen
#IDs anschauen und label für canvas

class Wiki2Manifest:
    wikicommons_api_template = "https://magnus-toolserver.toolforge.org/commonsapi.php?image={}"
    wikicommons_api_template2 = "https://commons.wikimedia.org/w/api.php?action=query" \
                                "&prop=imageinfo&format=json&iiprop=extmetadata&iilimit=5&titles=Image:{}"
    wiki_iiif_template = "https://ub-commonsiiif.ub.unibas.ch/v1/iiif/iiifimg/{}" #"https://zoomviewer.toolforge.org/proxy.php?iiif={}"
    lizenz_template = "https://lizenzhinweisgenerator.de/api/attribution/de/File:{}/online/unmodified"
    lookup_dict = {"title": "Titel",
                   "name": "Name",
                   "author": "Autor",
                   "source": "Quelle",
                   "width": "width",
                   "height": "height"}

    def __init__(self, img_name):
        self.img_name = img_name

    @CacheDecorator12h()
    def get_wiki_license(self, img_name):
        license_url = self.lizenz_template.format(img_name)
        license_data = requests.get(license_url).json()
        return license_data

    @staticmethod
    @CacheDecorator12h()
    def get_wikicommons_data(img_name):
        wikicommonsdata = requests.get(Wiki2Manifest.wikicommons_api_template2.format(img_name)).json()
        pages_data = list(wikicommonsdata.get("query",{}).get("pages").values())
        metadata = pages_data[0].get("imageinfo")[0]["extmetadata"]
        pageid = pages_data[0].get("pageid")
        iiif_metadata = {"Wikicommons Url": f"<a href='https://commons.wikimedia.org/w/index.php?search={img_name}'>Link Wikimedia Commons</a>"}
        lookup_field = {"Wikicommons Url": 1, "Credit":1, "Artist": 1, "ObjectName": 1, "DateTimeOriginal":1,
                        "ImageDescription":1, "LicenseUrl": 1, "LicenseShortName":1}
        for key, values in metadata.items():
            v = values.get("value", "")
            if key in ["ObjectName"] and re.search("<i>.*?</i>", v):
                iiif_metadata["ObjectName"] = re.search("<i>(?P<title>.*?)</i>", v).groupdict().get("title")
            elif key in ["DateTimeOriginal"]:
                iiif_metadata["DateOriginal"] = re.sub("<div.*</div>", "", v)
            elif key in lookup_field:
                iiif_metadata[key] = v
        return iiif_metadata

    @CacheDecorator12h()
    def get_wikicommons_data_old(self, img_name):
        wikicommonsdata = requests.get(self.wikicommons_api_template.format(img_name)).text
        root = ElementTree.fromstring(wikicommonsdata)
        iiif_metadata = {}
        for filesec in root.findall("file"):
            for elem in filesec:
                label = self.lookup_dict.get(elem.tag, "")
                if label:
                    value = elem.text
                    if label in ["width", "height"]:
                        value = int(value)
                    iiif_metadata.setdefault(label, value)
        for filesec in root.findall("description"):
            for elem in filesec:
                if elem.attrib.get("code") == "default":
                    iiif_metadata.setdefault("Beschreibung", elem.text)
        for filesec in root.findall("categories"):
            for elem in filesec:
                iiif_metadata.setdefault("Kategorien", []).append(elem.text)
        for filesec in root.findall("licenses"):
            for elem in filesec:
                for name in elem:
                    iiif_metadata.setdefault("Lizenzen", []).append(name.text)
        return iiif_metadata

    def build_wiki_manifestfromdb(self, config_store, host):
        es_host = config_store.get_value('hosts.index')
        index = config_store.get_value('databases.index_name')
        es = Elasticsearch(es_host)

        img_name_urlencoded = urllib.parse.quote(self.img_name)
        results = es.search(index=index, query={"terms": {"wikicommons_file_name.keyword": [img_name_urlencoded]}})

        #print(self.img_name, img_name_urlencoded)
        iiif_data = results.get("hits",{}).get("hits",{})[0]["_source"]

        cleansed_img_name = ".".join(self.img_name.replace('_', ' ').split(".")[0:-1])
        manifest = Manifest(id=f"{host}",
                            label=f"Bild eingebunden von Wikimedia Commons: {cleansed_img_name}",
                            summary={"de":[iiif_data.get("wikicommons_description", "")]})
        if iiif_data.get("attributionHtml"):
            manifest.rights = iiif_data.get("licenseUrl")
            manifest.requiredStatement = KeyValueString(label={"de": ["Rechte"]}, value=iiif_data.get("attributionHtml"))
        else:
            manifest.requiredStatement = KeyValueString(label={"de": ["Rechte"]}, value="Rechte Statement konnte nicht via https://lizenzhinweisgenerator.de/api generiert werden, "
                                                                                        "siehe auch https://github.com/wmde/attribution-generator-api/issues/39 "
                                                                                        "Bitte auf Wikicommons nachschauen.")

        metadata_lookup = {"wikicommons_url": "WikiCommons Link", "wikicommons_credit": "Credit",
                        "wikicommons_artist": "Künstler*in", "wikicommons_objectname": "Objektbezeichnung",
                        "wikicommons_date": "Entstehung", "wikicommons_description": "Beschreibung"}
        manifest.metadata = [KeyValueString(label={"de": [metadata_lookup.get(k, k)]}, value=v)
                             for k, v in iiif_data.items() if k in metadata_lookup]

        iiif_imgs = iiif_data.get("iiif_imgs")
        iiif_img = iiif_imgs[0]
        iiif_img = iiif_img.replace("https://ub-commonsiiif.ub.unibas.ch//v1/iiif/iiifimg/{}", "https://zoomviewer.toolforge.org/proxy.php?iiif={}")
        height = iiif_img.get("height")
        width = iiif_img.get("height")

        iiif_service = self.wiki_iiif_template.format(img_name_urlencoded)
        canvas = Canvas(id=f"{iiif_service}/canvas", height=height, width=width)
        service = ServiceItem(id=iiif_service, profile="http://iiif.io/api/image/2/level2.json", type="ImageService2")
        anno_body = ResourceItem(id=f'{iiif_service}/full/max/0/default.jpg',
                                 type="Image",
                                 format="image/jpeg",
                                 service = [service],
                                 height=height,
                                 width=width)
        anno_page = AnnotationPage(id=f"{iiif_service}/anno_page")
        anno = Annotation(id=f"{iiif_service}/anno",
                          motivation="painting",
                          body=anno_body,
                          target=canvas.id)
        anno_page.add_item(anno)
        canvas.add_item(anno_page)
        manifest.add_item(canvas)
        return manifest

    def get_wiki_manifest(self, host):
        iiif_metadata2 = self.get_wikicommons_data(img_name=self.img_name)
        rights_statement = self.get_wiki_license(img_name=self.img_name)

        config.configs['helpers.auto_fields.AutoLang'].auto_lang = "en"
        cleansed_img_name = ".".join(self.img_name.replace('_', ' ').split(".")[0:-1])
        manifest = Manifest(id=f"{host}",
                            label=f"Bild eingebunden von Wikimedia Commons: {cleansed_img_name}",
                            summary={"de":[iiif_metadata2.get("ImageDescription", "")]})

        if not rights_statement.get("error"):
            manifest.rights = rights_statement.get("licenseUrl")
            manifest.requiredStatement = KeyValueString(label={"de": ["Rechte"]}, value=rights_statement.get("attributionHtml"))
        else:
            manifest.requiredStatement = KeyValueString(label={"de": ["Rechte"]}, value="Rechte Statement konnte nicht via https://lizenzhinweisgenerator.de/api generiert werden, "
                                                                                        "siehe auch https://github.com/wmde/attribution-generator-api/issues/39 "
                                                                                        "Bitte auf Wikicommons nachschauen.")

        manifest.metadata = [KeyValueString(label={"de": [k]}, value=v) for k, v in iiif_metadata2.items()]
        manifest.make_canvas_from_iiif(url=self.wiki_iiif_template.format(self.img_name))
        manifest.items[0].items[0].items[0].body.id = self.wiki_iiif_template.format(self.img_name)
        manifest.items[0].items[0].items[0].body.service[0].id = self.wiki_iiif_template.format(self.img_name)
        return manifest

if __name__ == "__main__":
    img_name = "Deutsche_Geschichte5-310.jpg"
    wiki = Wiki2Manifest(img_name)
    print(wiki.get_wiki_manifest(host="https://localhost").json(indent=2))





